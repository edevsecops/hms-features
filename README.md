# DEPLOY RAWBANK-AJUBA THROUGH THE CICD

---

## TABLE OF CONTENT

- [CI-PROCESS-VERIFICATION](#CI-PROCESS-VERIFICATION)
- [REPOSITORY-ACCESS](#REPOSITORY-ACCESS)
- [STRUCTURE-OF-THE-DA-CONFIG-REPOSITORY](#STRUCTURE-OF-THE-DA-CONFIG-REPOSITORY)
- [CONFIG-AND-ENV-CHANGES](#CONFIG-AND-ENV-CHANGES)
- [CONFIGURE-THE-MODULE-VERSION-CHANGES](#CONFIGURE-THE-MODULE-VERSION-CHANGES)
- [DB-SCRIPT-CHANGES](#DB-SCRIPT-CHANGES)
- [CONFIGURE-THE-SIMULATOR-CHANGES](#CONFIGURE-THE-SIMULATOR-CHANGES)
- [DEPLOY-THE-SIMULATORS](#DEPLOY-THE-SIMULATORS)
- [MODULES-ON-&-OFF](#MODULES-ON-&-OFF)
- [TEST-RUNS-BEFORE-RELEASE-THE-ACTUAL-TAG](#TEST-RUNS-BEFORE-RELEASE-THE-ACTUAL-TAG)
- [RELEASE-THE-TAG](#RELEASE-THE-TAG)
- [LOCAL-CHANGES](#LOCAL-CHANGES)
- [COMMIT-LOCAL-CHANGES-TO-THE-REPOSITORY](#COMMIT-LOCAL-CHANGES-TO-THE-REPOSITORY)
- [ROLLBACK](#ROLLBACK)
- [CD-PROCESS-VERIFICATION](#CD-PROCESS-VERIFICATION)


<a name="CI-PROCESS-VERIFICATION"></a>
## CI PROCESS VERIFICATION

You can be verified the CI process using CI pipeline URL which comes under the `CI Pipeline Status` secction in the Dev Release note under the `rawbank/ajuba`. 

Once you click the CI Pipeline Status URL, You can see 4 stages consisting of 4 buttons related to the tag which the dev team released. You can verify the image publish part whether is it success or not.

stage names are as follows:

1. pre-eve-evaluation-stage
2. build-jar-files
3. build-docker-images
4. publish


<a name="REPOSITORY-ACCESS"></a>
## REPOSITORY ACCESS

First, you need to check the release note provided by the dev team, what are the changed modules for the relevant tag.  Then you should apply those changes through the `ajuba-da` config repository to the DA environment.

[https://sourcecontrol.hsenidmobile.com/core/rawbank/ajuba-da](https://sourcecontrol.hsenidmobile.com/core/rawbank/ajuba-da).

You need to checkout to `da-config` branch to do configuration changes related to the tag.


<a name="STRUCTURE-OF-THE-DA-CONFIG-REPOSITORY"></a>
## STRUCTURE OF THE DA-CONFIG REPOSITORY

`da-config` branch has following example folder structure.

```bash

ajuba-da
├── ajuba-apps
│   ├── ajuba-3.0
│   │   └── conf
│   │       ├── ajuba.env
│   │       ├── ajuba.properties
│   │       ├── database.properties
│   │       ├── distribution.properties
│   │       ├── in-connector-context.xml
│   │       ├── in-connector.properties
│   │       ├── log4j2.xml
│   │       ├── repositories.xml
│   │       ├── sdp-connector-context.xml
│   │       ├── smpp.properties
│   │       ├── spring-context.xml
│   │       ├── ussd-context.xml
│   │       ├── ussd-gateways.json
│   │       └── wrapper.conf
│   └── menu_designer
│       └── menu-designer
│           └── WEB-INF
│               └── classes
│                   ├── action-continuation-mapping.json
│                   ├── localize.json
│                   ├── log4j2.xml
│                   ├── mail.properties
│                   ├── menu-designer.env
│                   ├── menu-designer-parameter-options.json
│                   ├── menu-designer.properties
│                   ├── menu-designer-reloadable-properties.json
│                   ├── nashorn-poly.js
│                   ├── parameter-type-mapping.properties
│                   ├── simulator-allowed-msisdns.json
│                   ├── snmp_messages.properties
│                   ├── template
│                   │   ├── dailyApproval.vm
│                   │   ├── mailMapping.json
│                   │   ├── mailOnApprove.vm
│                   │   ├── mailOnCreate.vm
│                   │   ├── mailOnReject.vm
│                   │   ├── mailOnSubmit_approver.vm
│                   │   └── mailOnSubmit_editor.vm
│                   └── ui-customizations.json
├── ajuba-installs
│   └── db-setup
│       ├── init-scripts
│       │   ├── ajuba_initial_data.sql
│       │   ├── ajuba.sql
│       │   ├── common_admin.sql
│       │   ├── reporting.sql
│       │   └── spm.sql
│       ├── my.cnf
│       └── release-rawbank-x.x.x
│           └── update-script-x.x.x.sql
├── inventory
│   └── da.ini
├── podman
│   ├── playbook
│   │    ├── 11_deploy_ajuba_svc_pod.yaml
│   │    ├── 12_deploy_menu_designer_pod.yaml
│   │    ├── 6_configure_podman_registry.yaml
│   │    ├── 7_grafana_deployment.yaml
│   │    ├── 7_keycloak_deployment.yaml
│   │    ├── 7_node_exporter_deployment.yaml
│   │    └── 7_prometheus_deployment.yaml        
│   └── vars
│       ├── 11_deploy_ajuba_svc_pod.yaml
│       ├── 12_deploy_menu_designer_pod.yaml
│       ├── 6_configure_podman_registry.yaml
│       ├── 7_grafana_deployment.yaml
│       ├── 7_keycloak_deployment.yaml
│       ├── 7_node_exporter_deployment.yaml
│       └── 7_prometheus_deployment.yaml
├── docs
│   ├── cicd-instrucions.md
│   └── patch-creation-instructions.md
└── variables.yaml


```

Both `inventory` and `podman-->playbook` directories are related to the CICD process. Then don't edit or chanage files inside those directories.
And the docs folder contain all necessary information related to the process. 


<a name="CONFIG-AND-ENV-CHANGES"></a>
## CONFIG AND ENV CHANGES

The `ajuba-apps` directory contains all the env and configuration related files under each modules. All the files that are mapped into the container through volume mount should be included here.  In addition, the env file should also be included here. Each module in the project should have a directory in here and all mapping relevant files and folders should be here only (if the module has volume mapping or env file). You need to do the changes according to the merge requests which comes through the dev release note in here. Please find the `Patch Details` section in the dev release note to find the merge request URL.

1. navigate to the `ajuba-apps` directory to do the module configurations
2. find the module you want to do the configuration changes
3. edit the relevant files

If there any new module, you need to open a folder under the `ajuba-apps` directory and please include all the config and related files inside it. 


<a name="CONFIGURE-THE-MODULE-VERSION-CHANGES-AND-MANAGE-DOCKER-COMPOSE-FILE-CHANGES"></a>
## CONFIGURE THE MODULE VERSION CHANGES AND MANAGE DOCKER-COMPOSE FILE CHANGES

The `podman/var` directory contains podman container spec files and you should change the image tag of the container spec files related to the modules to be changed to the relevant tag which is in under the `image` attribute in the container spec file. As well as you need to modify podman var files if there any docker-compose files chanage in the dev env.

1. navigate to the `podman/vars` directory
2. select the container spec file you want to the changes
3. edit the relevant file


<a name="DB-SCRIPT-CHANGES"></a>
## DB SCRIPT CHANGES

Is there any database script change in the release you need to apply it manually.

You need to take database backups manually before do the db script chnages. Please update the `ajuba-da` config repo with the update script as mentioned in the directory structure.

1. create a directory named `release-rawbank-x.x.x` in the `ajuba-installs --> db-setup` path.
2. add the relavent db scripts to that directory


<a name="CONFIGURE-THE-SIMULATOR-CHANGES"></a>
## CONFIGURE THE SIMULATOR CHANGES

If there is a simulator change in the tag you want to release, the corresponding tags should be configured in the `variables.yaml` file in the root directory. 
```bash
variables:
  telco_sim: <tag>
```
You can find the tag of the telco_sim from the dev release note `Module Released` section under the `ajuba-rawbank` tag.


<a name="DEPLOY-THE-SIMULATORS"></a>
## DEPLOY THE SIMULATORS

From the `variables.yaml` file in the root directory, you can control the simulator deployed on DA servers and only the simulators that change should be `ON` status and all other simulators should be `OFF` status.

```bash
variables:
  telco_sim_deployment: "ON"
```

Once you release the tag from DA repository `telco zim` will be deployed through the pipeline to the app server desired location. In the pipeline you will be able to see following stages as follws.


1. ansible_deploy
2. telco_zim_app_layer - ( this is related to the telco-zim simulator deployment to the app layer )


The `wallet-api-sim` is deployed from seperate pipleine which is related to the `illico-connector` project. You can be verified the deployment of `wallet-api-sim` CICD process using CI pipeline URL which comes under the `CI Pipeline Status` secction in the Dev Release note under the `illico-connector`.

The locations where the simulators are deployed are as follows after you release the tag.


server details:
```bash
server_ip: 172.27.2.33
server_name: raw-bank-app
```

1. telco-sim
```bash
simulator_location: /hms/apps/simulators/telco-sim
```

2. openet-6d-sim
```bash
simulator_location: /hms/apps/simulators/wallet-api-sim
```

Please find the relavant tag to find the simulators.


<a name="MODULES-ON-&-OFF"></a>
## MODULES ON & OFF

From the `variables.yaml` file in the root directory, you can control the modules deployed on da servers and only the modules that change should be `ON` status and all other modules should be `OFF` status. Only the modules related to the release you have received can be deployed with this and the modules that have not changed can be maintained as they are.


```bash
variables:
  node_exporter: "OFF"
  prometheus: "OFF"
  grafana: "OFF"
  mariadb: "OFF"
  keycloak: "OFF"
  rawbank_ajuba: "OFF"
  menu_designer: "OFF"
```


<a name="TEST-RUNS-BEFORE-RELEASE-THE-ACTUAL-TAG"></a>
## TEST RUNS BEFORE RELEASE THE ACTUAL TAG

If you want to do a test deployment related to the patch you made (without releasing a tag from the git), follow the steps below.

1. Select `Build` from left menu bar
2. Choose the `pipelines`
3. Press `Run Pipeline`
4. Choose `test-deployments` branch
5. Then click the `Run Pipeline`

Then you can be seen following pipeline stages for each module as follow respectively;

1. registry_authentication
2. menu-designer
3. ajuba
4. keycloak
5. prometheus
6. grafana
7. node_exporter

You can confirm the status of each module by clicking the run button and running it. Please check the deployment status from server side as well.

<a name="RELEASE-THE-TAG"></a>
## RELEASE THE TAG

After completing all the configurations, you should release the relevant tag from the `ajuba-da` repository. Use the same tag which comes from dev side as the ajuba-da tag.

1. Select `Code` from left menu bar
2. Choose the `tags`
3. Select `New Tag`
4. Enter the tag name
5. Select `da-config` branch
6. Type any message you want to add a message to the tag
7. Select `Create tag`

Then your pipeline will automatically trigger and will proccced for the deployment.


<a name="LOCAL-CHANGES"></a>
## LOCAL CHANGES

If you need to do any changes reguarding the configurations while you are doing the da-testings you can use the following path to do your configurations. 

1. Log into relevant server
2. From server side navigate to the `/hms/ajuba-da` directory
3. checkout to the `da-config` branch using `git checkout da-config`
4. configure the relevant files you want

If you want to make local changes and give your changes to the modules, you must restart the relevant container at the end of the local changes. 


<a name="COMMIT-LOCAL-CHANGES-TO-THE-REPOSITORY"></a>
## COMMIT LOCAL CHANGES TO THE REPOSITORY

If you have any changes related to the configurations/env while you were doing your da-testings and if you want to commit thoses changes to the repository to use those changes to upcoming releases you can simply commit to the repo as follows. please use the `ajuba` user to commit local changes to the repository.


1. Log into relevant server
2. From server side navigate to the `/hms/ajuba-da` directory
3. checkout to the `da-config` branch
4. verify the changes you are going to commit to the ajuba da repo using `git status`
5. add the changes using `git add .`
6. type your commit message using `git commit -m "enter the commit message"`
7. push the local changes using `git push -u origin da-config`


<a name="ROLLBACK"></a>
## ROLLBACK

If you need to rollback to any perticular tag or version you just need to simplly press the `ansible_deploy` button from the `Ajuba-da` git repository pipeline section. follow the steps below.

1. Select `Build` from left menu bar
2. Select `Pipeline` under the build option
3. Go to the `tags`
4. select the tag you want to rollback
5. click on the pipeline ID which start from `#` left to the git tag
6. Press `Run Again` button in the `ansible_deploy` stage. 


<a name="CD-PROCESS-VERIFICATION"></a>
## CD PROCESS VERIFICATION

To verify your pipeline deployments, follow the steps below.

1. Select `Build` from left menu bar
2. Select `Pipeline` under the build option
3. Go to the `tags`
4. click on the pipeline ID which start from `#` left to the git tag you want to verify


Now you can see 2 stages consisting of 2 buttons related to the tag you released. You can verify your deployment whether is it success or not.

stage names are as follows:

`1. ansible_deploy`

`2. telco_sim_app_layer`


Click on the button name you want to check the log and you will be redirected to the pipeline job log. 

If all stages are successfully completed you can verify that the deployment process has been successfully executed according to the configurations which you configured in the config repo.






---
---


```bash
CREATED BY :
Kavishan Thathsara
Last Edit : 01/30/2024
```
