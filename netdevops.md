# Configure Internet Access On RHEL 9.2 Server

![Alt text](lab_diagram.png)

Connect RHEL server as the above diagram which created using RHEL 9.2 coustom image.

## Internal Communication

Configure following changes to communicate with the internal network

```bash
sudo vi /etc/sysconfig/network-scripts/ifcfg-eth0
```

```bash
TYPE=Ethernet
BOOTPROTO=static
BOOTPROTO=none
NAME=eth0
DEVICE=eth0
ONBOOT=yes
IPADDR=192.168.11.16
NETMASK=255.255.255.0
GATEWAY=192.168.11.1
DNS1=8.8.8.8
DNS2=8.8.4.4
```

## Configure Internet Access

```bash
Configure the following changes to communicate with outside network ( internet access ) 
```

```bash
TYPE=Ethernet
BOOTPROTO=static
BOOTPROTO=none
NAME=eth1
DEVICE=eth1
ONBOOT=yes
IPADDR=192.168.9.150
NETMASK=255.255.255.0
GATEWAY=192.168.9.2
DNS1=8.8.8.8
DNS2=8.8.4.4
```

## Verification

### Verify the internal connectivity

ping both routers and the switch

```bash
[root@localhost ~]# ping 192.168.11.254
PING 192.168.11.254 (192.168.11.254) 56(84) bytes of data.
64 bytes from 192.168.11.254: icmp_seq=1 ttl=255 time=7.59 ms
64 bytes from 192.168.11.254: icmp_seq=2 ttl=255 time=2.64 ms
64 bytes from 192.168.11.254: icmp_seq=3 ttl=255 time=2.69 ms
64 bytes from 192.168.11.254: icmp_seq=4 ttl=255 time=3.29 ms
^C
--- 192.168.11.254 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 2.640/4.052/7.594/2.060 ms
[root@localhost ~]# 
[root@localhost ~]# 
[root@localhost ~]# ping 192.168.11.1
PING 192.168.11.1 (192.168.11.1) 56(84) bytes of data.
64 bytes from 192.168.11.1: icmp_seq=1 ttl=255 time=6.24 ms
64 bytes from 192.168.11.1: icmp_seq=2 ttl=255 time=3.82 ms
64 bytes from 192.168.11.1: icmp_seq=3 ttl=255 time=3.63 ms
64 bytes from 192.168.11.1: icmp_seq=4 ttl=255 time=3.23 ms
^C
--- 192.168.11.1 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 3.229/4.230/6.241/1.180 ms
[root@localhost ~]# 
[root@localhost ~]# 
[root@localhost ~]# ping 192.168.11.2
PING 192.168.11.2 (192.168.11.2) 56(84) bytes of data.
64 bytes from 192.168.11.2: icmp_seq=1 ttl=255 time=6.09 ms
64 bytes from 192.168.11.2: icmp_seq=2 ttl=255 time=3.47 ms
64 bytes from 192.168.11.2: icmp_seq=3 ttl=255 time=4.24 ms
64 bytes from 192.168.11.2: icmp_seq=4 ttl=255 time=5.04 ms
64 bytes from 192.168.11.2: icmp_seq=5 ttl=255 time=3.12 ms
^C
--- 192.168.11.2 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4008ms
rtt min/avg/max/mdev = 3.116/4.389/6.087/1.076 ms
```

### Verify the Internet Connectivity

ping google.com and 8.8.8.8

```bash
[root@localhost ~]# ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=128 time=75.7 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=128 time=99.6 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=128 time=91.6 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 75.689/88.969/99.588/9.936 ms
[root@localhost ~]# 
[root@localhost ~]# ping 8.8.4.4
PING 8.8.4.4 (8.8.4.4) 56(84) bytes of data.
64 bytes from 8.8.4.4: icmp_seq=1 ttl=128 time=37.5 ms
64 bytes from 8.8.4.4: icmp_seq=2 ttl=128 time=35.4 ms
64 bytes from 8.8.4.4: icmp_seq=3 ttl=128 time=36.1 ms
^C
--- 8.8.4.4 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 35.350/36.315/37.507/0.895 ms
[root@localhost ~]# 
[root@localhost ~]# 
[root@localhost ~]# ping google.com
PING google.com (142.250.199.14) 56(84) bytes of data.
64 bytes from kul09s14-in-f14.1e100.net (142.250.199.14): icmp_seq=1 ttl=128 time=73.1 ms
64 bytes from kul09s14-in-f14.1e100.net (142.250.199.14): icmp_seq=2 ttl=128 time=71.4 ms
64 bytes from kul09s14-in-f14.1e100.net (142.250.199.14): icmp_seq=3 ttl=128 time=71.6 ms
64 bytes from kul09s14-in-f14.1e100.net (142.250.199.14): icmp_seq=4 ttl=128 time=71.7 ms
^C
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 71.438/71.952/73.057/0.644 ms
```